# ProtonVPN Simple GUI
ProtonVPN Simple GUI is a simple interface for ProtonVPN-CLI.<br>
<img src="https://i.imgur.com/KHe1mPT.png"><br>
The graphical interface eliminates the need to use commands.<br><br>
**ProtonVPN-CLI installation is required**<br><br>
Install it from the following link:<br>
https://github.com/ProtonVPN/protonvpn-cli-ng<br><br>

**How to run the graphical interface.**<br><br>
1 - Download [ProtonVPN.sh](https://gitlab.com/universales/protonvpn-simple-gui/-/raw/master/ProtonVPN.sh?inline=false)<br>
2 - Run it through the terminal 'sh ProtonVPN.sh'<br><br>
All ready :)<br><br>

**It is recommended to add execution permission to the script although it is not necessary.**

**Changelog**

**V0.3** - Now it doesn't take long to request the password when connecting to the fastest servers.

**V0.2** - Added the possibility of installing ProtonVPN-CLI updates, added option to install ProtonVPN-CLI if it is not installed.

**V0.1** - Initial version.
