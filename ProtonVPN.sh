#!/bin/bash

inicio(){
opcion=$(whiptail --title "ProtonVPN Minimal GUI | V0.3" --menu "Select an option in the menu:" 15 60 8 \
"1" "Initialize ProtonVPN profile." \
"2" "Connect to a ProtonVPN server." \
"3" "Disconnect the current session." \
"4" "Print connection status." \
"5" "Change CLI configuration." \
"6" "Refresh OpenVPN configuration and server data." \
"7" "Update ProtonVPN-CLI version." \
"8" "Display version."  3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then    
    if [ $opcion = "1" ]; then
       sudo protonvpn init;
       inicio;
    fi
    if [ $opcion = "2" ]; then
        menu2;
    fi
    if [ $opcion = "3" ]; then
       desconectadovpn=$(sudo protonvpn disconnect);
       mensajeinfo "$desconectadovpn";
       inicio;
    fi
    if [ $opcion = "4" ]; then
       estadovpn=$(protonvpn status);
       mensajeinfo "$estadovpn";
       inicio;
    fi
    if [ $opcion = "5" ]; then
       sudo protonvpn configure;
       inicio;
    fi
    if [ $opcion = "6" ]; then
       protonvpn refresh;
       mensajeinfo "Updated ProtonVPN server status.";
       inicio;
    fi
    if [ $opcion = "7" ]; then
       actualizarvpn=$(sudo pip3 install protonvpn-cli --upgrade);
       mensajeinfo "$actualizarvpn";
       inicio;
    fi
    if [ $opcion = "8" ]; then
       versionvpn=$(protonvpn --version);
       mensajeinfo "$versionvpn";
       inicio;
    fi
else
    exit 1;
fi
}

menu2(){
opcion=$(whiptail --title "ProtonVPN Minimal GUI | V0.3" --menu "Select an option in the menu:" 15 60 6 \
"1" "Select a ProtonVPN server and connect to it." \
"2" "Connect to a random server." \
"3" "Connect to the fastest server." \
"4" "Connect to the fastest P2P server." \
"5" "Connect to the fastest Secure Core server." \
"6" "Reconnect or connect to the last server used."  3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    if [ $opcion = "1" ]; then
       sudo protonvpn c;
       menu2;
    fi
    if [ $opcion = "2" ]; then
       randomvpn=$(sudo protonvpn c -r);
       mensajeinfo "$randomvpn";
       inicio;
    fi
    if [ $opcion = "3" ]; then
       sudo protonvpn refresh;
       fastestvpn=$(sudo protonvpn c -f);
       mensajeinfo "$fastestvpn";
       inicio;
    fi
    if [ $opcion = "4" ]; then
       sudo protonvpn refresh;
       fastestp2pvpn=$(sudo protonvpn c --p2p);
       mensajeinfo "$fastestp2pvpn";
       inicio;
    fi
    if [ $opcion = "5" ]; then
       sudo protonvpn refresh;
       fastestscsvpn=$(sudo protonvpn c --sc);
       mensajeinfo "$fastestscsvpn";
       inicio;
    fi
    if [ $opcion = "6" ]; then
       reconnectvpn=$(sudo protonvpn reconnect);
       mensajeinfo "$reconnectvpn";
       inicio;
    fi    
else
    inicio;
fi
}

mensajeinfo(){
whiptail --title "ProtonVPN Minimal GUI | V0.3" --msgbox "$1" 30 60
}

instalarprotoncli(){
opcion=$(whiptail --title "ProtonVPN Minimal GUI | V0.3" --menu "Select an option in the menu:" 15 60 1 \
"1" "Install ProtonVPN-CLI."  3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    if [ $opcion = "1" ]; then
       installvpn=$(sudo pip3 install protonvpn-cli);
       mensajeinfo "$installvpn";
       if [ -n "$(type protonvpn)" ]; then
            inicio;
       else
            exit 1;
       fi
    fi
else
    exit 1;
fi
}

if [ -n "$(type protonvpn)" ]; then
    inicio;
else
    mensajeinfo "ProtonVPN-CLI is not installed on your system, please install it to continue.";
    instalarprotoncli;
fi
exit 1;